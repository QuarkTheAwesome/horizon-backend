#!/usr/bin/env python3


import subprocess


def set_locale(locale, use_localegen=False):
    if use_localegen:
        # For glibc, generate the locale database
        with open("/etc/locale.gen", "w") as f:
            # TODO - support other charmaps
            f.write("{}.UTF-8 UTF-8".format(locale))
            try:
                subprocess.run(("locale-gen",), stdout=subprocess.DEVNULL)
            except FileNotFoundError as e:
                print("WARN: locale-gen missing, could not generate glibc locales")

    try:
        subprocess.run(("eselect", "locale", "set", locale + ".UTF-8"))
    except FileNotFoundError as e:
        print("WARN: eselect missing, could not set locale")
