#!/usr/bin/env python3

import subprocess


def add_mirror(mirror):
    with open("/etc/repositories", "a") as f:
        # FIXME
        f.write(mirror + "/adelie")


def bootstrap():
    # This should install the apk tools and stuff.
    raise NotImplementedError


def install_package(package, pkgargs=None):
    args = ["apk", "add"]

    if pkgargs:
        args.extend(pkgargs)
    
    if not isinstance(package, str):
        args.extend(package)
    else:
        args.append(package)

    p = subprocess.popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return p

