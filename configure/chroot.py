#!/usr/bin/env python3

import os


root_handle = os.open("/", os.O_RDONLY)


def chroot_target():
    os.makedirs("/target", exist_ok=True)
    os.chroot("/target")


def unchroot():
    os.fchdir(root_handle)
    os.chroot(".")
