#!/usr/bin/env python3

import subprocess
import os


def mount(device, path, rbind=False, fs=None, options=None):
    args = ["mount"]

    if options:
        args.append("-o")

        optlist = []
        for option in options:
            if isinstance(option, str):
                optlist.append(option)
            else:
                optlist.append(option.join("="))

        args.append(",".join(optlist))

    if rbind:
        args.extend(["--rbind"])

    if fs:
        args.extend(["-t", fs])

    args.append(device)
    args.append(path)

    subprocess.run(args)
