#!/usr/bin/env python3

import subprocess


# This provides uniform mappings between names... not used atm but can be
# later.
service_trans = {}


def __service_configure(service, runlevel, verb):
    if service in service_trans:
        service = service_trans[service]

    return subprocess.run(("rc-update", verb, service, runlevel))

def __service_add(service, runlevel):
    return __service_configure(service, runlevel, "add")

def __service_del(service, runlevel):
    return __service_configure(service, runlevel, "del")


def service_add_default(service):
    return __service_add(service, "default")

def service_add_boot(service):
    return __service_add(service, "boot")

def service_del_default(service):
    return __service_del(service, "default")

def service_del_boot(service):
    return __service_del(service, "boot")
