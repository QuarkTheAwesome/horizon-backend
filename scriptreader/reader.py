#!/usr/bin/env python3

import subprocess
import shutil
import os

from configure import locale, package, timezone, chroot
from configure.initsystem import openrc
from configure.bootloader import grub2
from configure.partition import mount


class ScriptError(Exception):
    def __init__(self, name, error):
        self.name = name
        self.error = error


class UnknownVerbError(ScriptError):
    pass


class InvalidMountError(ScriptError):
    pass


class NoInitError(ScriptError):
    pass


class ScriptReader:

    def __init__(self, path):
        self.path = path
        self._mounted = {}
        self._initsystem = None

    def execute(self):
        with open(self.path, "r") as f:
            for line in f.readlines():
                line = line.strip()
                if not line or line[0] == "#":
                    continue

                # Find any in-line comments and remove them
                cindex = line.find("#")
                if cindex > -1:
                    # Remove trailing whitespace too
                    line = line[:cindex].rstrip()

                verb, _, line = line.partition(" ")
                line = line.strip()  # Just in case

                try:
                    fn = getattr(self, "cfg_" + verb.lower())
                except Exception:
                    raise UnknownVerbError(self.path,
                                           "Verb {} is unknown".format(verb))

                try:
                    fn(line)
                finally:
                    # Precautionary measure
                    chroot.unchroot()

    def cfg_language(self, line):
        chroot.chroot_target()
        locale.set_locale(line)

    cfg_lang = cfg_language

    def cfg_partition(self, line):
        raise NotImplementedError

    def cfg_mount(self, line):
        params = line.split()

        if params[1] != "/" and "/" not in self._mounted:
            raise InvalidMountError(self.path,
                                    "Cannot mount {} until / "
                                    "is mounted".format(params[1]))
        elif params[1] in self._mounted:
            raise InvalidMountError(self.path,
                                    "{} is already mounted".format(params[1]))

        if not params[0].startswith("/dev"):
            device = fstype = params[0]
        elif len(params) > 2:
            device = params[0]
            fstype = params[2]
        else:
            device = params[0]
            fstype = None

        chroot_mountpoint = params[1]
        mountpoint = "/target{}".format(chroot_mountpoint)
        os.makedirs(mountpoint, exist_ok=True)
        mount.mount(device, mountpoint, fs=fstype)

        self._mounted[chroot_mountpoint] = device

        if chroot_mountpoint == "/":
            # Automatic mounts
            os.makedirs("/target/proc", exist_ok=True)
            os.makedirs("/target/sys", exist_ok=True)
            os.makedirs("/target/dev", exist_ok=True)
            mount.mount("proc", "/target/proc", fs="proc")
            mount.mount("sysfs", "/target/sys", fs="sysfs")
            mount.mount("/dev", "/target/dev", rbind=True)

            self._mounted["/proc"] = "proc"
            self._mounted["/sys"] = "sysfs"
            self._mounted["/dev"] = "bind"

            # Needed for name lookups
            os.makedirs("/target/etc", exist_ok=True)
            shutil.copy("/etc/resolv.conf", "/target/etc/resolv.conf")

    def cfg_pkginstall(self, line):
        package.bootstrap()

        chroot.chroot_target()

        packages = list(filter(None, line.split()))
        if "openrc" in packages:
            self._initsystem = openrc
        else:
            raise InitError(self.path, "No init system selected!")

        package.install_package(line.split())

    def cfg_shell(self, line):
        subprocess.run(line, shell=True, stdin=subprocess.DEVNULL,
                       stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    def cfg_bootloader(self, line):
        chroot.chroot_target()

        line = list(filter(None, line.split()))
        loader = line[0].lower()
        installdevs = line[1:]

        if loader == "grub":
            bootloader = grub2
        else:
            raise NotImplementedError

        bootloader.install(installdevs)

    def cfg_init(self, line):
        line = list(filter(None, line.split()))

        verb = line[0].lower()
        runlevel = line[1].lower()
        service = line[2]

        attr = "service_{}_{}".format(verb, runlevel)
        fn = getattr(self._initsystem, attr)

        chroot.chroot_target()
        fn(service)
